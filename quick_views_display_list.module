<?php
/**
 * @file
 * Simplifies the Views - Display list.
 *
 * Since each display requires 3 to 5 clicks, this module makes the 
 * processeasier and faster by providing direct links to each display tab.
 */


/**
 * Implements hook_init().
 */
function quick_views_display_list_init() {
  if ( current_path() == 'admin/structure/views' ) {
  drupal_set_message( t('<div>See your views and displays faster on the ' . l(t('Quick Views Display List'), 'admin/config/development/quick-views-display-list') . '</div>'));
  }
}

/**
 * Implements hook_help().
 */
function quick_views_display_list_help($path, $arg) {
  switch ($path) {
  case "admin/help#quick_views_display_list":
  return t('<p><b>
		Where The Quick Views Display List Is Available
		</b>
		</p>

		<p>
		This table is available from either a block or a configuration item under Development section.
		</p>

		<p><b>
		About This List
		</b>
		</p>

		<p>
		Views are one of the most powerful features for organizing content in Drupal, 
		but sometimes, you may just want a quick link to your views page and its corresponding displays without clicking all the links to get there.
		</p>

		<p>
		This module will display a simple table of views and links to their displays in just one click. 
		</p>

		<p><b>
		Special Note About Views In Code
		</b>
		</p>


		<p>
		Any view that is stored in the database will display. 
		For views that are exported to code and reverted, 
		the module will list any view which includes a file ending 
		in views_default.inc, the common file name for exported views. 
		Views stored in code from any other method will not be listed.
		</p>') ;
  }
}


/**
 * Implements hook_block_info().
 */
function quick_views_display_list_block_info() {
  $blocks['quick_view_display_list'] = array(
  'info' => t('Quick View Displays'),
  );

  return $blocks;
}


/**
 * Implements hook_menu().
 */
function quick_views_display_list_menu() {

  $items['admin/config/development/quick-views-display-list'] = array(
  'title' => 'Quick Views Display List',
  'description' => 'Get a list of all views and their displays with links to each.',
  'page callback' => 'list_table',
  'access arguments' => array('administer users'),  
  'type' => MENU_NORMAL_ITEM, 
  );

  return $items;
}


/**
 * Implements hook_block_view().
 */
function quick_views_display_list_block_view($delta) {
  $block = "";
  
  switch ($delta) {
  case 'quick_view_display_list':
  $block['subject'] = t('A Quick List of All Views');
  $block['content'] = list_table() ;
  break;
  }

  return $block;
}



/**
 * Creates the table of links.
 */
function list_table() {

  global $base_url;
  drupal_add_css(drupal_get_path('module', 'quick_views_display_list') . '/quick_views_display_list.css');

  $list =  "";


$link_views_main = l(t('Main Views Page'), 'admin/structure/views');
$link_views_db = l(t('Views In Database'), 'admin/config/development/quick-views-display-list', array('fragment' => 'views-in-database', 'external' => FALSE));
$link_views_code = l(t('Views In Code'), 'admin/config/development/quick-views-display-list', array('fragment' => 'views-in-code', 'external' => FALSE));
$link_views_help = l(t('Help'), 'admin/help/quick_views_display_list');

  $list .= ("$link_views_main | $link_views_db | $link_views_code | $link_views_help");


  $list .=filter_xss('
				<h3 id="views-in-database">Views In Database</h3>
				<div class="views-quick-table-wrapper" >
				<div class="views_table_main">
					<div class="left">
						View Name
					</div>
					<div  class="right">
						Display Name
					</div>
				</div>
			', array('div', 'h3'));

  try {
  $result = db_query("
					SELECT v.name, v.human_name, d.display_title, d.id 
					FROM {views_view} v
					inner join {views_display} d
					on v.vid=d.vid 
					order by v.human_name, d.display_title
					");
  $row = 0;
  foreach ($result as $record) {
  
  $view_mn = $record->name;
  $display_mn = $record->id;
  $view_name = $record->human_name;
  $display_name = $record->display_title;
  $view_link = l(t($view_name), 'admin/structure/views/view/' . $view_mn . '/edit/', array('html' => TRUE));
  $display_link = l(t($display_name), 'admin/structure/views/view/' . $view_mn . '/edit/' . $display_mn, array('html' => TRUE));
  $list .=  filter_xss("
						<div class='view-links-main'>
							<div class='left'>
								$view_link
							</div>
							<div class='right'>
								$display_link
							</div>
						</div>
						", array('div', 'a'));
  }
  
  $list .= "</div>";

  }

  catch (PDOException $e) {
  $list .= t("No views in the database have been detected.");
  }


  $list .= filter_xss("
			<h3 id='views-in-code' >Views In Code</h3>
			<div class='views-quick-table-wrapper' >

			<div class='views_table_main'>
				<div class='left'>
					View Name
				</div>
				<div  class='right'>
					Display Name
				</div>
			</div>
		", array('h3', 'div'));


  foreach (module_implements('views_api') as $mod) {
  $dir = str_replace("index.php", '', $_SERVER["SCRIPT_FILENAME"]) . "sites/all/modules/$mod/";


  /*
	We want to look for any file that has "views_default.inc".
	When a file with this subset is found, open it to read various lines.
  */

  $view_file = "$dir/$mod.views_default.inc";
  $file_handle = "";

  if (file_exists($view_file)) {
  $file_handle = fopen($view_file, "r");
  while (!feof($file_handle)) {
  $line = fgets($file_handle);

  if (strpos($line, '$view->name')>-1) {
  $viewmname = str_replace('$view->name = \'', "", $line);
  $viewmname = trim(str_replace('\';', "", $viewmname));
  }

  if (strpos($line, '$view->human_name')>-1) {
  $viewtitle = str_replace('$view->human_name = \'', "", $line);
  $viewtitle = str_replace('\';', "", $viewtitle);
  $view_link = l(t($viewtitle), 'admin/structure/views/view/' . $viewmname . '/edit/');  
  }

  if (strpos($line, '$handler = $view->new_display')>-1) {
  $display = str_replace('$handler = $view->new_display', "", $line);
  $display = str_replace("(", "", $display);
  $display = str_replace(");", "", $display);
  $display = str_replace("'", "", $display);
  $display_array = explode(',', $display);
  $display_title = $display_array[1];
  $display_mn = trim($display_array[2]);
  $display_link = l(t($display_name), 'admin/structure/views/view/' . $viewmname . '/edit/' . $display_mn);

  $list .= filter_xss("
	<div class='view-links-main'>
	<div class='left'>
		$view_link
	</div>
	<div class='right'>
		$display_link
	</div>
	</div>
  ", array('a', 'div') );
  }
  }

  $list .= "</div>";
  }
  }

  if ($file_handle) {
  fclose($file_handle);
  }
  return $list;
}

