INTRODUCTION
----------------------
The Quick Views Display List is a quick and dirty list of all views and 
their associated displays. It is beneficial to views that have many 
displays. Each view hyperlink links to the main view page and each 
display hyperlink links to its tab so editing of the display can be 
further edited.

Sometimes, getting to the exact display takes between 3 to 5 
clicks, so this module was created to simplify number of clicks 
and access the exact display you need. Also, for long display 
names, the tabs are usually cut off, so this list is great for that 
purpose.

FOR VIEWS IN CODE
-------------------------------
The module will not only find views in the database, but also 
views that are exported to code. In order coded views to be 
listed, the code must reside in an external file 
with "VIEW_NAME.views_default.inc", which is the standard 
way of using exported views. The view must also implement 
hook_views_api() in a module related to each coded view.

AVAILABLE AS A CONFIGURATION PAGE OR BLOG
-------------------------------------------------------------------------
For troubleshooting purposes during development of any Drupal 
website, a block is also available, but for better security, the list 
can be accessed in the Development section of the 
Configuration page.

REQUIREMENTS
------------------------------
This module requires the following modules:
 * Views (https://drupal.org/project/views)
 * CTOOLS (https://drupal.org/project/ctools)

INSTALLATION
--------------------
 * Install as you would normally install a contributed drupal 
    module through the Drupal admin or Drush. 

TROUBLESHOOTING
-----------------------------
 * This module will list all views in the database.
 * For views that are in code which do NOT show, make sure
   the view uses hook_views_api() and the exported code is 
   located in a views_default.inc.

MAINTAINERS
--------------------
Current maintainers:
 * Bruce Chamoff (hwi1) - https://drupal.org/u/hwi2
